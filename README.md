# Discovery Server Sample

Running from CLI
 
	$ java -jar discovery-service-0.0.1.jar
	
or, Running main method class 'DiscoveryServiceApplication.java' from IDE or CLI

Its configured to run on port '8761' 
This auto register with service clients which has DiscoveryClient enabled.

#Eureka

To access eureka server page
 
	http://${host}:8761/

To get all registered services

	http://${host}:8761/eureka/apps

# Docker
This is configured to build docker image and push to private nexus repository using maven plugin.
	
	mvn clean -X package docker:build -DpushImage

Maven properties

	docker.image.prefix=<nexus host>:18079
	docker.registry.id=<Docker Repository name>
	registry.url=<registry url e.g. https://192.168.1.98:8449/repository/myDockerRepo/ >
	
Running under docker container

	$>docker run -p <avaible port>:<Eureka server port> -t <tag name>/discovery-service:0.0.1
	e.g docker run -p 8761:8761 -t ubuntuVB:18079/discovery-service:0.0.1